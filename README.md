# Prueba Desarrollador Front-end

Este proyecto se realizó utilizando el comando `npx create-react-app` .
Se conforma por el login, catálogo con filtros y logout, las vistas están diseñadas para desktop y smartphone.
Adicionalmente se trabajó la barra de navegación, rutas privadas y custom hooks

## Instalación

Para la instalación del proyecto de utiliza el comando `npm install`

## Scripts

### `npm start`

Ejecuta la aplicación en entorno de desarrollo.
Abrir http://localhost:3000 para verla en el navegador

## Construido con 🛠️

- React js
- Axios
- react-router-dom

## Estructura del proyecto

Se compone por 5 carpetas ubicadas en la carpeta src

#### Assets

Todo lo correspondiente a imágenes

#### Componentes

Es la parte esencial de la aplicación.
Allí se encuentran las carpetas con el respectivo nombre de cada componente y dentro se ubican los archivos correspondientes al componente (con extensión `.jsx`) y el archivo de estilos de ese componente

#### Config

Son archivos que se encargan de configuraciones necesarias para la ejecución del proyecto, pero sin llegar a manejar información sensible.

- localStorageConst : Es el archivo que guarda el nombre (key) del elemento que se guarda en el local storage
- routes: Se encuentran todas las rutas que contendrá la aplicación

#### Hooks

En esta carpeta se encuentran los custom hooks

- useFetchData
- useForm

#### Styles

Contiene los estilos generales de la aplicación

## .Env

La variable de entorno REACT_APP_WON_API hace referencia a la url de la API a consumir

## Autor ✒️

Diego Forero
