import axios from 'axios';
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { USER_TOKEN } from '../../config/localStorageConst';
import { catalog } from '../../config/routes';
import { useForm } from '../../hooks/useForm';
import { Input } from '../Input/Input';
import { Loader } from '../Loader/Loader';
import { LoaderLogin } from './LoaderLogin/LoaderLogin';
import './Login.css';

export const Login = () => {
  const [state, setState] = useState({ loading: false, data: null, error: null });

  const [formValues, handleInputChange] = useForm({ email: '', password: '' });
  const { email, password } = formValues;

  const handleSubmit = async (e) => {
    e.preventDefault();
    setState({ data: null, error: null, loading: true });
    try {
      const res = await axios.post(process.env.REACT_APP_WON_API + '/login', { email, password, type: 'User' });
      const { data } = res.data;
      localStorage.setItem(USER_TOKEN, data.token);
      setState({ ...state, loading: false, data });
    } catch (error) {
      setState({ ...state, loading: false, error });
    }
  };

  if (localStorage.getItem(USER_TOKEN)) {
    return <Redirect to={catalog()} />;
  }

  if (state.loading) {
    return (
      <div className='container-fluid'>
        <div className='row vh-100 justify-content-center align-items-center'>
          <div className='col-10 mx-auto'>
            <Loader />
          </div>
        </div>
      </div>
    );
  }

  return (
    <section className='container-fluid'>
      <div className='row vh-100 justify-content-center bg-login align-items-center'>
        <LoaderLogin />
        <form className='col-lg-4 login-form p-4  card' onSubmit={handleSubmit}>
          <h2 className='login-form__title'>Iniciar sesión</h2>
          <Input handleInputChange={handleInputChange} value={email} name='email' type='email' text='Correo Electronico' />
          <Input handleInputChange={handleInputChange} value={password} name='password' type='password' text='Contraseña' />
          <a href='./' className='login-form__forgotPassword my-4'>
            Olvidé la contraseña
          </a>
          <button className='btn-primary col-7 mx-auto d-block mt-5' type='submit'>
            Ingresar
          </button>
          {state.error && <p className='login-form__incorrectCredentials'>El correo o contraseña son erroneos</p>}

          <hr className='col-7 my-4 mx-auto' />
          <a href='./' className='btn-secondary mx-auto col-7 d-block'>
            Crear cuenta
          </a>
        </form>
      </div>
    </section>
  );
};
