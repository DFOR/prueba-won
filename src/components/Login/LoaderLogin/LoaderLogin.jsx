import React from 'react';
import './LoaderLogin.css';

import LogoLogin from '../../../assets/Won-logo-login.svg';
import LogoLoginMobile from '../../../assets/won-login-mobile.svg';

export const LoaderLogin = () => {
  return (
    <>
      <div className='mr-auto layer-login'></div>
      <img src={LogoLogin} alt='' className='logo-login' />
      <img src={LogoLoginMobile} alt='' className='logo-login-mobile' />
    </>
  );
};
