import React from 'react';
import './Input.css';

export const Input = ({ styleClass = 'mx-auto col-9', handleInputChange, value = '', name, type = 'text', text = 'Custom Input' }) => {
  return (
    <div className={`input-group-underlined ${styleClass}`}>
      <input required name={name} type={type} onChange={handleInputChange} value={value} autoComplete='on' />
      <label htmlFor={name} className='input-group-underlined__label'>
        {text}
      </label>
    </div>
  );
};
