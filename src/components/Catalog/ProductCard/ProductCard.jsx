import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import './ProductCard.css';
import { product as productRoute } from '../../../config/routes';

export const ProductCard = ({ product }) => {
  const [redirect, setRedirect] = useState(false);

  if (redirect) {
    return (
      <Redirect
        to={{
          pathname: productRoute(product.id),
          product,
        }}
      />
    );
  }

  return (
    <div className='productCard' onClick={() => setRedirect(true)}>
      <img className='productCard__image' src={product.image_url} alt='' />
      <p className='productCard__name'>{product.name.toLowerCase()}</p>
      <p className='productCard__value'> $ {Intl.NumberFormat('de-DE').format(product.value)}</p>
      <div className='productCard__points'>
        <p>Obtinenes {product.points === 1 ? product.points + ' punto' : product.points + ' puntos'}</p>
      </div>
    </div>
  );
};
