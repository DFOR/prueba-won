import React, { useState } from 'react';
import { useFetchData } from '../../hooks/useFetchData';
import { ProductCard } from './ProductCard/ProductCard';
import { Loader } from '../Loader/Loader';
import { ProductFilter } from './ProductFilter/ProductFilter';

export const Catalog = () => {
  const [urlData, setUrlData] = useState('/products/top');
  const { data, loading, error } = useFetchData(urlData);

  const handleFilter = (category) => {
    if (category === '') {
      return setUrlData(`/products/top`);
    }
    setUrlData(`/products/search?category=${category}&page=1&per_page=8`);
  };

  if (error) {
    return alert('Hubo un error');
  }

  return (
    <>
      <ProductFilter handleFilter={handleFilter} />

      {loading ? (
        <Loader />
      ) : (
        <div className='col-12 justify-content-around mt-4'>
          <div className='row px-4'>
            {data.map((product) => (
              <ProductCard key={product.id} product={product} />
            ))}
          </div>
        </div>
      )}
    </>
  );
};
