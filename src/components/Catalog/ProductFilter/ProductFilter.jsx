import React, { useRef, useState } from 'react';

import './ProductFilter.css';
export const ProductFilter = ({ handleFilter }) => {
  const filterList = [
    {
      name: 'Top 10',
      category: '',
    },
    {
      name: 'Apps',
      category: '11',
    },
    {
      name: 'Bonos',
      category: '6',
    },
    {
      name: 'Paquetes',
      category: '2',
    },
    {
      name: 'Recargas',
      category: '1',
    },
  ];

  const [scrollPosition, setScrollPosition] = useState('left');
  const marker = useRef();

  function indicator(e, category) {
    if (!marker.current) {
      return null;
    }

    marker.current.style.left = e.target.offsetLeft + 'px';
    marker.current.style.width = e.target.offsetWidth + 'px';

    handleFilter(category);
  }
  const ref = useRef(null);

  const scroll = (varrr) => {
    if (varrr === 'right') {
      setScrollPosition('rigth');
      return (ref.current.scrollLeft = ref.current.scrollWidth);
    }
    setScrollPosition('left');

    return (ref.current.scrollLeft = -ref.current.scrollWidth);
  };
  return (
    <nav className='productFilter-nav col-12  mx-auto'>
      <button className={scrollPosition === 'left' ? 'd-none' : 'productFilter__btn-scroll-left'} onClick={() => scroll('left')}>
        ᐸ
      </button>
      <ul ref={ref}>
        <div id='marker' ref={marker}></div>
        {filterList.map((item, index) => (
          <button onClick={(e) => indicator(e, item.category)} key={'item-filter-list' + index} className='productFilter-nav__button'>
            {item.name}
          </button>
        ))}
      </ul>
      <button className={scrollPosition === 'rigth' ? 'd-none' : 'productFilter__btn-scroll-rigth'} onClick={() => scroll('right')}>
        ᐳ
      </button>
    </nav>
  );
};
