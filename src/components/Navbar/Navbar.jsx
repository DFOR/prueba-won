import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { navbarLinks } from './navbarLinks';

import Logo from '../../assets/Won-logo-login.svg';

import './Navbar.css';

export const Navbar = () => {
  const [toggle, setToggle] = useState(false);

  const ACTIVE = { backgroundColor: 'var(--secondary)' };
  return (
    <nav className='navbar'>
      <div className='navbar-button' onClick={() => setToggle(!toggle)}>
        <span className={toggle ? 'active' : ''}></span>
      </div>

      <div className={toggle ? 'navbar-content active' : 'navbar-content'}>
        <img src={Logo} alt='' />

        <ul>
          {navbarLinks.map((val, index) => (
            <NavLink key={'navbar-item-' + index} to={val.link} className='navlink' activeStyle={ACTIVE} onClick={() => setToggle(!toggle)}>
              {val.icon}
              <span> {val.title}</span>
            </NavLink>
          ))}
        </ul>
      </div>
    </nav>
  );
};
