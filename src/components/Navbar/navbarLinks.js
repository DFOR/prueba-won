import { catalog, logout, bonus, office, network, invite, wonedero, notifications, legal } from '../../config/routes';
import { Icon } from '../Icon/Icon';

export const navbarLinks = [
  {
    title: 'Oficina',
    icon: <Icon svg='office' classes='svg-nav-icon' />,
    link: office(),
  },

  {
    title: 'Catalogo',
    icon: <Icon svg='catalog' classes='svg-nav-icon' />,
    link: catalog(),
  },
  {
    title: 'Bonos',
    icon: <Icon svg='bonus' classes='svg-nav-icon' />,
    link: bonus(),
  },
  {
    title: 'Red',
    icon: <Icon svg='network' classes='svg-nav-icon' />,
    link: network(),
  },
  {
    title: 'Invitar',
    icon: <Icon svg='invite' classes='svg-nav-icon' />,
    link: invite(),
  },
  {
    title: 'Wonedero',
    icon: <Icon svg='wallet' classes='svg-nav-icon' />,
    link: wonedero(),
  },
  {
    title: 'Notificaciones',
    icon: <Icon svg='notifications' classes='svg-nav-icon' />,
    link: notifications(),
  },
  {
    title: 'Legal',
    icon: <Icon svg='legal' classes='svg-nav-icon' />,
    link: legal(),
  },
  {
    title: 'Cerrar Sesión',
    icon: <Icon svg='logout' classes='svg-nav-icon' />,
    link: logout(),
  },
];
