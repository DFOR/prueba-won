import React from 'react';

import svgs from './icons';
import './Icon.css';

export const Icon = ({ svg, classes = 'svg-icon', title, click }) => {
  const iconRender = svgs[svg] || svgs.default;
  return (
    <svg viewBox={iconRender.viewBox} className={classes} title={title} xmlns='http://www.w3.org/2000/svg'>
      {iconRender.svg}
    </svg>
  );
};
