import React, { useEffect, useState } from 'react';
import { USER_TOKEN } from '../../config/localStorageConst';
import { Icon } from '../Icon/Icon';

import './Logout.css';
export const Logout = () => {
  localStorage.removeItem(USER_TOKEN);

  const [countDown, setCountDown] = useState(4);

  useEffect(() => {
    let time;
    if (countDown > 0) {
      time = setTimeout(() => {
        setCountDown(countDown - 1);
      }, 1000);
    }

    return function cleanUp() {
      clearTimeout(time);
    };
  }, [countDown]);

  if (countDown === 0) {
    return window.location.reload();
  }

  return (
    <div className='d-flex vh-100 align-items-center'>
      <div className='col logout-container'>
        <Icon svg='sadFace' classes='svg-logout-icon' />
        <p className='logout-container__title'>Sesión Expirada</p>
        <span className='logout-container__description'>Te extrañamos, vuelve pronto</span>
        <span className='logout-container__countdown'>Serás redirijido al login en {countDown}</span>
      </div>
    </div>
  );
};
