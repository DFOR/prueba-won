import { Redirect } from 'react-router-dom';
import { USER_TOKEN } from '../../config/localStorageConst';
import { login } from '../../config/routes';

import './PrivateRoute.css';

export const PrivateRoute = ({ Component, location, props }) => {
  if (!localStorage.getItem(USER_TOKEN)) {
    return <Redirect to={login()} />;
  }

  return (
    <div id='content'>
      <Component location={location} {...props} />
    </div>
  );
};
