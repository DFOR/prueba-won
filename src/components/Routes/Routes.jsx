import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { catalog, login, logout } from '../../config/routes';
import { Catalog } from '../Catalog/Catalog';
import { Login } from '../Login/Login';
import { Logout } from '../Logout/Logout';
import { Navbar } from '../Navbar/Navbar';
import { PrivateRoute } from '../PrivateRoute/PrivateRoute';

export const Routes = () => {
  return (
    <Switch>
      <Route exact path={login()} component={Login} />
      <Route exact path='/' component={Login} />

      <>
        <Navbar />
        <Route exact path={catalog()} component={(props) => <PrivateRoute {...props} Component={Catalog} />} />
        <Route exact path={logout()} component={(props) => <PrivateRoute {...props} Component={Logout} />} />
      </>
    </Switch>
  );
};
