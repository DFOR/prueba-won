export function bonus() {
  return '/bonos';
}

export function catalog() {
  return '/catalog';
}

export function office() {
  return '/oficina';
}

export function login() {
  return '/login';
}

export function network() {
  return '/red';
}

export function invite() {
  return '/invitar';
}
export function wonedero() {
  return '/wonedero';
}

export function notifications() {
  return '/notificaciones';
}

export function legal() {
  return '/legal';
}

export function logout() {
  return '/logout';
}

export function product(id) {
  return `/catalog/product/:${id}`;
}
